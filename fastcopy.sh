# This Shell script is designed to run from a Linux host and has been tested on a CentOS 7 server

# The following 7 variables need to be set for this script to work
DATADOMAIN='fqdn'               # FQDN or IP address of Data Domain; DATADOMAIN='datadomain.domain.local'
DDACCOUNT='account'             # The Data Domain account used to create the SSH certificate configuration; eg 'fastcopy'
APPMTREE='/data/col1/.…'        # Data Domain MTRee location used by the Application; APPMTREE='/data/col1/test'
RLMTREE='/data/col1/.…/'        # Data Domain MTree location of the 'Retention Lock' MTRee; RLMTREE='/data/col1/test-rl/' 
NFSMOUNT='/mnt/nfs/….'          # NFS export on Linux host of 'Retention Lock' MTree on the Data Domain; NFSMOUNT='/mnt/nfs/test-rl'
OUTPUT='/…/…./'                 # Directory location of the output files; OUTPUT='/tmp/script_output/test/'
RLDAYS=14                       # Retention period in days as defined in the Data Domain 'automatic retention period' parameter for the 'Retention Lock' MTRee; RLDAYS=14
# Note that a backslash is required at the end of the RLMTREE and OUTPUT variables

# Grabbing the current date/time and assigning it to the variable DATE
DATE=$(date +%Y%m%d-%H:%M)

# Echo the DATE stamp to the script-log text file
echo "Date and time Format to be used: $DATE" > "$OUTPUT$DATE"-script-log.txt

# Echo the Fast Copy command of the application MTree (APPMTREE) to the 'Retention Lock' MTree (RLMTREE) in the script-log text file
echo "Fast Copy of $APPMTREE to $RLMTREE$DATE" >> "$OUTPUT$DATE"-script-log.txt

# Create a Fast Copy of the application MTree (APPMTREE) and save it to the 'Retention Lock' MTree (RLMTREE) via a SSH connection
# Output from this command is captured to the script-log text file
ssh $DDACCOUNT@$DATADOMAIN filesys fastcopy source $APPMTREE destination $RLMTREE$DATE >> "$OUTPUT$DATE"-script-log.txt

# Echo Fast Copy directories that were created more than the number of days as specified in variable (RLDAYS) for log purposes
echo "Finding directories in $RLMTREE created move than $RLDAYS days ago" >> "$OUTPUT$DATE"-script-log.txt

# Find Fast Copy directories that were created more than the number of days as specified in variable (RLDAYS)
find $NFSMOUNT -maxdepth 1 -type d -ctime +$RLDAYS ! -path '*snapshot*' -printf '%p\n' >> $OUTPUT$DATE-directories.txt

# Check to see if the directories file contains any directory listings
if [ -s $OUTPUT$DATE-directories.txt ]
	then 
		echo "$OUTPUT$DATE-directories.txt contains directory listing" >> "$OUTPUT$DATE"-script-log.txt
		# Delete the directories found and listed in the $OUTPUT$DATE-directories.txt files
		# Every directory found is read and then deleted
		cat $OUTPUT$DATE-directories.txt | while read LINE
		do
			echo "Found directory: " $LINE >> "$OUTPUT$DATE"-script-log.txt
			rm -rf $LINE
			# running $? command to get he output of the above listed rm command.
			# If a 0 is returned, the directory was successful deleted
			# if a 1 is returned, the the deletion command was not completed successfully
			if [ $? -eq 0 ]
				then
					echo "Successfully deleted directory: " $LINE >> "$OUTPUT$DATE"-script-log.txt
				else
					echo "Failed to deleted directory: " $LINE >> "$OUTPUT$DATE"-script-log.txt
			fi
		done
	else
		echo "No directories found" >> "$OUTPUT$DATE"-script-log.txt
	fi

echo "End of script: " $DATE >> "$OUTPUT$DATE"-script-log.txt

# end of script